-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2016 at 10:45 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `class`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE IF NOT EXISTS `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime NOT NULL,
  `date_modify` datetime NOT NULL,
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `line_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `original_file_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_file_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`member_id`, `date_created`, `date_modify`, `firstname`, `lastname`, `nickname`, `no`, `tel`, `line_id`, `email`, `address`, `original_file_name`, `new_file_name`, `view_count`, `active`, `deleted`) VALUES
(1, '2016-06-03 00:00:00', '2016-06-08 17:39:48', 'Thanapat', 'Vattanakrai', 'numton', '25443', '0899993451', 'numton', 'numton@gmail.com', '470 changkland Road', 'unnamed.png', '20160608103948_thumb.png', 12, 1, 0),
(2, '2016-06-02 00:00:00', '2016-06-08 17:40:23', 'Jukkrit', 'Last', 'Tor', '4451244', '0820015451', 'tor2016', 'tor16@gmail.com', '154 chiangmai', 'shadow_fiend_illustration__dota_2_by_tgnow-d8u7kqh.jpg', '20160608104023_thumb.jpg', 2, 1, 0),
(3, '2016-06-02 00:00:00', '2016-06-08 18:00:48', 'Ladatip', 'Nannan', 'Tip', '564222', '0898530012', 'nongtip', 'tipzz@gmail.com', '78 chiangmai', 'unnamed.png', '20160608110048_thumb.png', 3, 1, 0),
(4, '2016-06-02 00:00:00', '2016-06-06 15:19:08', 'Waraporn', 'Longdo', 'Meaw', '96441', '0865421103', 'mmlove', 'mmlove@gmail.com', '112/2 chiangmai', 'd4561a2e20c4949355026ce7f2f15ad8.jpg', '20160606081908_thumb.jpg', 6, 1, 0),
(5, '2016-06-02 00:00:00', '2016-06-06 15:19:14', 'Jutiporn', 'Rampage', 'Wan', '11255', '0965554125', 'rampage007', 'rr007@gmail.com', '99 chiangmai', 'dota2__lina_and_crystal_maiden_by_zerox_ii-d97gi7j.jpg', '20160606081914_thumb.jpg', 1, 1, 0),
(6, '2016-06-02 00:00:00', '2016-06-06 15:19:20', 'Kongcon', 'Saisai', 'Jame', '241112', '0852253362', 'jamexd', 'jamex@gmail.com', '21 chiangmai', 'dota2-page-fg-e1408395653809.png', '20160606081920_thumb.png', 3, 1, 0),
(19, '2016-06-02 16:33:11', '2016-06-08 17:32:24', 'Pinid', 'Vattanakrai', 'nid', '12265', '08172414730', '221eee', 'numooon@gmail.com', '32156841', '521181441_preview_yurnero.jpg', '20160608103224_thumb.jpg', 0, 1, 0),
(78, '2016-06-09 10:24:16', '2016-06-09 10:24:16', 'q', 'q', 'q', 'q', 'q', 'q', 'q@q.com', 'q', 'shadow_fiend_illustration__dota_2_by_tgnow-d8u7kqh.jpg', '20160609032416_thumb.jpg', 0, 1, 0),
(76, '2016-06-09 09:49:05', '2016-06-09 09:49:05', '45', '5q', '45q45', 'q45', '4q5', '4q5', '4q54', '5q', '521181441_preview_yurnero.jpg', '20160609024905_thumb.jpg', 0, 1, 0),
(77, '2016-06-09 09:50:56', '2016-06-09 09:50:56', '455', '45', '4', '54', '54', '5', '45', '45', 'unnamed.png', '20160609025056_thumb.png', 0, 1, 0),
(74, '2016-06-09 09:19:11', '2016-06-09 09:47:31', '11111', '11111111', '11111111', '111111111', '111111111', '1111111111', '111111111@hdsa.com', '111111111111111', 'unnamed.png', '20160609024731_thumb.png', 0, 1, 0),
(75, '2016-06-09 09:33:28', '2016-06-09 09:33:28', 'erg', '', '', '', '', '', '', '', 'unnamed.png', '20160609023328_thumb.png', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` datetime DEFAULT NULL,
  `date_modify` datetime DEFAULT NULL,
  `latest_login` datetime DEFAULT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `date_created`, `date_modify`, `latest_login`, `username`, `password`, `email`) VALUES
(1, '2016-06-06 16:25:00', '2016-06-06 16:25:00', '2016-06-08 17:39:01', 'admin1', 'e10adc3949ba59abbe56e057f20f883e', 'admin1@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
