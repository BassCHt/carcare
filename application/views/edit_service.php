<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />-->

<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/global/plugins/datatables/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />



<?php $this->load->view('head_menu'); ?>

<?php
if ($service_id = $_GET['id']) {
    
} else {
    $service_id = 0;
}

if (isset($_GET['status'])) {
    if ($_GET['status'] == 1) {
        echo "<input id='status' type='hidden' value='1'>";
    } else {
        echo "<input id='status' type='hidden' value='0'>";
    }
} else {
    $_GET['status'] = 0;
}

foreach ($detail as $row) {
    $service_id = $row->service_id;
    $service = $row->service;
    $price = $row->price;
    $category = $row->category;
}
echo '<input id="hidden_cr" type="hidden" value="'.$category.'">';
?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Edit product</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div id="notification_box">

                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">

                                            <!--nav bar-->
                                            <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
                                                <li class="active"><a href="#product" data-toggle="tab">บริการ</a></li>
                                                <li><a href="#variant" data-toggle="tab">ตัวเลือกบริการ</a></li>
                                            </ul>
                                            <div id="my-tab-content" class="tab-content">
                                                <!--product-->
                                                <div class="tab-pane active" id="product"><br><br>
                                                    <div class="page-content-inner">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="portlet light ">
                                                                    <div class='form-group'>
                                                                        <label class='control-label'>บริการ</label> 
                                                                        <input id='inp_service' type='text' class='form-control' value="<?php echo $service; ?>"> 
                                                                    </div>
                                                                    <div id="sl_box" class="form-group"> 
                                                                        <label class="control-label">ประเภท</label>   
                                                                        <select id="sl_category" class="form-control">
                                                                            
                                                                        </select>
                                                                    </div>
                                                                    <div class='form-group'>
                                                                        <label class='control-label'>ราคา</label>
                                                                        <input id='inp_price' type='text'  class='form-control' value="<?php echo $price; ?>"> 
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class='margiv-top-10'>
                                                                <center>
                                                                    <button type='button' id='save_edit' p_id="<?php echo $service_id; ?>" style="width:100px;" class='btn green'> บันทึกรายการ </button>
                                                                    <button type='button' id='cancle' style="width:100px;"  class='btn btn-link'> ยกเลิก </button>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--product-->

                                                <!--variant-->
                                                <div class="tab-pane" id="variant">
                                                    <br>
                                                    <div class="page-content-inner">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="portlet light ">
                                                                    <div class="portlet-title">
                                                                        <a href="<?php echo base_url('service_variant/add?id=' . $service_id . ''); ?>">
                                                                            <button type="button" id="variant_add" style="padding-top:8px" class="btn green"><i class="fa fa-plus"></i> เพิ่มตัวเลือกบริการ </button>
                                                                        </a>
                                                                        <div class="tools"> </div>
                                                                    </div>
                                                                    <div class="portlet-body">
                                                                        <table class="table table-striped table-bordered table-hover" id="variant_list_table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th> ตัวเลือกบริการ </th>
                                                                                    <th> ราคา </th>
                                                                                    <th> รายละเอียด </th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody id="tbody_variant">

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--variant-->
                                            </div>
                                            <!--nav bar-->                                   


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->

</div>

<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/pages/scripts/profile.min.js'); ?>" type="text/javascript"></script>


<script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/lib/markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/form-validation.min.js'); ?>" type="text/javascript"></script>

<link href="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/global/scripts/datatable.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/table-datatables-rowreorder.min.js'); ?>" type="text/javascript"></script>

<script>

    function hidden_box() {
        $(document).ready(function () {
            $("#notification_box").fadeOut("slow");
        });
    }
    setTimeout("hidden_box()", 2000);

    $(document).ready(function () {

        $('#tabs').tab();

//        data table variant

        var table = $('#variant_list_table');
        var oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            "sAjaxSource": "<?php echo site_url('service/variant_list_ajax?id=' . $service_id . ''); ?>",
            "bServerSide": true,
            "sServerMethod": "POST",
            "bSort": false,
            "ordering": false,
            'fnServerData': function (sSource, aoData, fnCallback) {

                $.ajax({
                    url: sSource,
                    type: 'POST',
                    cache: false,
                    data: aoData,
                    dataType: 'jsonp',
                    jsonp: 'jsoncallback',
                    success: fnCallback
                });
            },
            'fnDrawCallback': function (oSettings) {
            },
            // setup buttons extentension: http://datatables.net/extensions/buttons/
            buttons: [
                {extend: 'print', className: 'btn dark btn-outline'},
                {extend: 'pdf', className: 'btn green btn-outline'},
                {extend: 'csv', className: 'btn purple btn-outline '}
            ],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });

//        data table variant

        var status = $("#status").val();
        if (status == 1) {
            $("#notification_box").append("<div class='note note-success'><span class='bold'><i class='fa fa-check' aria-hidden='true'></i> SUCCESS </span><span>&nbsp แก้ไขตัวเลือกสินค้าเรียบร้อย</span> </div>");
        } else if (status == 0) {
            $("#notification_box").append("<div class='note note-success'><span class='bold'></i> ERROR! </span><span>&nbsp Failed </span> </div>");
        }

        $("#save_edit").click(function () {
            var service_id = $("#save_edit").attr("p_id");
            var service = $("#inp_service").val();
            var category = $("#sl_category").val();
            var price = $("#inp_price").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage_service/update_service/'); ?>",
                data: {
                    service_id: service_id,
                    service: service,
                    category: category,
                    price: price
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                console.log(data);
                if (data == 1) {
                    $("#inp_service").val(service);
                    $("#inp_category").val(category);
                    $("#inp_price").val(price);
                    window.location.href = "<?php echo base_url('service?&status=1'); ?>";
                    //$("#notification_box").append("<div class='note note-success'><span class='bold'><i class='fa fa-check' aria-hidden='true'></i> SUCCESS </span><span>&nbsp Product has updated</span> </div>");
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });

        var hidden_cr = $("#hidden_cr").val();
        console.log(hidden_cr);
        $.ajax({
            type: "GET",
            url: "<?php echo base_url('manage_service/category/'); ?>",
            dataType: "text",
            cache: false,
        }).done(function (data) {
            var json_obj = JSON.parse(data);
            //console.log(json_obj);
            $.each(json_obj, function (key, cr) {
                $("#sl_category").append('<option value="'+cr.service_category_id+'" class="custom-font">'+cr.category+'</option>');
                $("div#sl_box select").val(hidden_cr);
            });
        });
        


    });
</script>
<?php $this->load->view('footer_2'); ?>