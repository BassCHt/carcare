<?php $this->load->view('header_1'); ?>
<link rel="stylesheet" href="<?php echo base_url('assets/touch-punch/jquery-ui.css'); ?>" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<link href="<?php echo base_url('assets/bootstrap-switch-master/bootstrap-switch.css'); ?>" rel="stylesheet">


<style>

    .list {
        background-color: white;
        /*        font-size: 30px;
                text-align: center;*/
        cursor: pointer;
        border: 1px solid gray;
    }

    #items .ui-selected {
        background: #444d58;
        color: white;
        font-weight: bold;
    }

    #items {
        list-style-type: none;
        margin: 0;
        padding: 0;
    }

    #items li {
        float: left;
        /*        margin: 10px;
                padding: 10px;*/
        margin: 10px;
        width: 207px;
        height: 330px;
        line-height: 48px;
    }

    .highlight {
        border: 1px solid #5d6b7a;
        font-weight: bold;
        /*font-size: 45px;*/
        /*background-color: lightblue;*/
    }

    #content{float: right; width: 100%; height:100%;}
    .box-1{width:100%; height:190px;  float:left}
    .box-2{width:100%; height:45px; background:#5d6b7a;color: #ced5de; float:left;}
    .box-3{width:100%; height:45px; background:#444d58;color: #f1f1f1; float:left;}
    #footer{background: #666666}
    #frame{ width:100%; height:100%;}


</style>

<?php $this->load->view('header_2'); ?>
<?php $this->load->view('head_menu'); ?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>จัดการสินค้า</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div id="notification_box">

                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light " >
                                            <select>
                                                <option value="volvo"><a href="#" >สินค้าทั้งหมด (99<?php //echo $row;                     ?>)</a></option>
                                                <option value="saab">Saab</option>
                                                <option value="mercedes">Mercedes</option>
                                                <option value="audi">Audi</option>
                                            </select>
                                            <div style="float: right">
                                                <button id="add_product" type="button" style="font-size: 20px;width:150px;height:40px" class="btn btn-success"><i class="fa fa-plus"></i> เพิ่มสินค้า</button>
                                            </div>
                                            <div style="padding-bottom: 14px"></div>
                                            <div id="hidden_box" class=""><hr>
                                                <form action="<?php echo base_url('product/add/'); ?>" method="post">                            
                                                    <div class="">
                                                        <h4 style="color:#444d58">เพิ่มสินค้าใหม่</h4>
                                                    </div><!--end .form-header-->
                                                    <div class="form-group">
                                                        <label>ชื่อสินค้า:</label>
                                                        <input id="product_name" name="product_name" type="text" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>ราคาตั้ง:</label>
                                                        <input id="price" name="price" type="text" class="form-control">
                                                    </div>
                                                    <div class="form-actions">
                                                        <button type="submit" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                        <button id="cancle" type="button" class="btn btn-link">ยกเลิก</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div align="center" >
                                            <ul id="items">
                                                <?php
                                                foreach ($detail as $row) {
                                                    ?>
                                                    <li class="list">
                                                        <div id="frame">
                                                            <div id="content">
                                                                <center>
                                                                    <a style="margin:0px;padding:0px;" href="<?php echo base_url('product/detail/' . $row->product_id . '') ?>">
                                                                        <input style="width:93%;" type="button" class="btn default" value="แก้ไข">
                                                                    </a>
                                                                </center>
                                                                <div class="box-1">
                                                                    <?php if ($row->img != "" || $row->img != NULL) { ?>
                                                                        <img src="<?php echo base_url('assets/img/upload/' . $row->img . ''); ?>" width="93%">
                                                                    <?php } else { ?>
                                                                        <img src="<?php echo base_url('assets/img/no-image.png'); ?>" width="93%">
                                                                    <?php } ?>
                                                                </div>

                                                                <div class="box-2"style="text-align: center;"><div style="float: left;width:50%;"><?php echo $row->product_name; ?></div ><div><?php echo '฿ ' . $row->price; ?></div></div>
                                                                <div class="box-3">
                                                                    <?php if ($row->status == 1) { ?>
                                                                        <input type="checkbox" name="my-checkbox" pd-id="<?php echo $row->product_id; ?>" data-on-text="<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>" data-off-text="<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>" data-size="mini" data-on-color="btn btn-success" data-off-color="btn btn-danger" checked>
                                                                    <?php } else { ?>
                                                                        <input type="checkbox" name="my-checkbox" pd-id="<?php echo $row->product_id; ?>" data-on-text="<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>" data-off-text="<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>" data-size="mini" data-on-color="btn btn-success" data-off-color="btn btn-danger">
                                                                    <?php } ?>
                                                                </div>                                                               
                                                            </div>                                                                                                               
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                        <div id="info" style="font-family:Geneva,Arial,Helvetica,sans-serif;font-size:20px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>
    <!-- END QUICK SIDEBAR -->
</div>

<?php $this->load->view('footer_1'); ?>
<script type="text/javascript" src="<?php echo base_url('assets/touch-punch/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/touch-punch/jquery-ui.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/touch-punch/jquery.ui.touch-punch.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/bootstrap-switch-master/bootstrap-switch.js'); ?>"></script>

<script>
    $("[name='my-checkbox']").bootstrapSwitch();

    $(function () {
        $("#items").sortable({
            placeholder: "highlight",
            start: function (event, ui) {
                ui.item.toggleClass("highlight");
            },
            stop: function (event, ui) {
                ui.item.toggleClass("highlight");
            }
        });
        $("#items").disableSelection();
    });

    $(document).ready(function () {

        $("#hidden_box").hide();

        $("#add_product").click(function () {
            $("#hidden_box").slideToggle("slow");
        });

        $("#cancle").click(function () {
            $("#hidden_box").slideToggle("slow");
        });


        $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function (event, state) {
            //console.log(state); 
            var product_id = $(this).attr("pd-id");
            //console.log(status);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('product/active'); ?>",
                data: {
                    product_id: product_id,
                    state: state  
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {

            });

        });



    });

</script>
<?php $this->load->view('footer_2'); ?>