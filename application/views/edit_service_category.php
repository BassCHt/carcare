<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/jquery.fine-uploader/fine-uploader-new.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />

<?php $this->load->view('head_menu'); ?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>บริการ | ประเภทตัวเลือกบริการ
                        <small>แก้ไขประเภทตัวเลือกบริการ</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->

                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">รายละเอียดประเภทตัวเลือกบริการ</span>
                                                </div>
                                            </div>
                                            <?php foreach ($detail as $row) { 
                                                $cr = $row->category;
                                                $svcr_id = $row->service_category_id;
                                            
                                            
                                            }?>
                                                <div class="portlet-body">
                                                    <div class="tab-content">
                                                        <div class='form-group'>
                                                            <label class='control-label'>ประเภทตัวเลือกบริการ</label> 
                                                            <input id='category' type='text' class='form-control' value="<?php echo $cr; ?>"> 
                                                        </div>

                                                        <div class='margiv-top-10'>
                                                            <center>
                                                                <button type='button' id='save_edit' c_id="<?php echo $svcr_id; ?>" style="width:100px;" class='btn green'> Save </button>
                                                                <a id="pd_id" href="<?php echo base_url('service/category'); ?> "  >
                                                                    <button type='button' id='cancle' style="width:100px;" class='btn btn-link'> Cancel </button>
                                                                </a>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->

</div>

<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/pages/scripts/profile.min.js'); ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/jquery.fine-uploader/jquery.fine-uploader.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/lib/markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/form-validation.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script>


    $(document).ready(function () {

        $("#save_edit").click(function () {
            var service_category_id = $("#save_edit").attr("c_id");
            var category = $("#category").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage_service/update_category/'); ?>",
                data: {
                    service_category_id: service_category_id,
                    category: category
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                if (data == 1) {
                    $("#category").val(category);
                    window.location.href = "<?php echo base_url('service/category?status=1'); ?>";
                    
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });





    });

</script>
<?php $this->load->view('footer_2'); ?>