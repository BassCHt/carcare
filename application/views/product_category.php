<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />-->

<link href="<?php echo base_url('assets/global/plugins/datatables/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/jquery.fine-uploader/fine-uploader-new.css'); ?>" rel="stylesheet" type="text/css"/>
<script type="text/template" id="qq-template-manual-trigger">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
    </div>
    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
    <span class="qq-upload-drop-area-text-selector"></span>
    </div>
    <div class="buttons">
    <div class="qq-upload-button-selector qq-upload-button" style="width:120px">
    <div>Select files</div>
    </div>
    <button type="button" id="trigger-upload" style="width:120px" class="btn btn-primary">
    <i class="icon-upload icon-white"></i> Upload
    </button>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
    <span>Processing dropped files...</span>
    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
    <li>
    <div class="qq-progress-bar-container-selector">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
    </div>
    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
    <span class="qq-upload-file-selector qq-upload-file"></span>
    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
    <span class="qq-upload-size-selector qq-upload-size"></span>
    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
    </li>
    </ul>

    <dialog class="qq-alert-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector">Close</button>
    </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector">No</button>
    <button type="button" class="qq-ok-button-selector">Yes</button>
    </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <input type="text">
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector">Cancel</button>
    <button type="button" class="qq-ok-button-selector">Ok</button>
    </div>
    </dialog>
    </div>
</script>

<style>

    a {
        text-decoration: none !important;
    }

    #trigger-upload {
        color: white;
        background-color: #00ABC7;
        font-size: 14px;
        padding: 7px 20px;
        background-image: none;
    }

    #fine-uploader-manual-trigger .qq-upload-button {
        margin-right: 15px;
    }

    #fine-uploader-manual-trigger .buttons {
        width: 36%;
    }

    #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
        width: 60%;
    }

    .img_box{
        width:120px;
        border-style: solid;
        border-color:#e1e5ec;
        border-width: 1px;
    }

</style>

<?php $this->load->view('head_menu'); ?>

<?php
foreach ($category as $rs) {
    $store_id = $rs->store_id;
}
echo '<input id="hidden_store_id" type="hidden" value="' . $store_id . '">';
?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>รายละเอียดสินค้า</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div id="notification_box">

                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">


                                            <!--stock-->
                                            <div class="tab-pane" id="stock">
                                                <br>
                                                <div class="page-content-inner">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div>
                                                                <h4 style="color:#444d58">ประเภทสินค้า</h4>
                                                                <div id="out_vrbox">
                                                                    <?php foreach ($category as $rr) { ?>

                                                                        <div id="in_vrbox<?php echo $rr->product_category_id; ?>" style="width: 100%; height: 70px;color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;">
                                                                            <div style="padding-top:13px;padding-left:25px;float:left;width:40%;" >
                                                                                <font id="name_vr_<?php echo $rr->product_category_id; ?>" size="3px" style="font-weight: 600 !important;"><?php echo $rr->category; ?></font>
                                                                                <br>
                                                                                <font size="2px" color="#7A7A7A">ประเภทสินค้า</font>
                                                                            </div>
                                                                            <div style="padding-top:13px;float:left;width:30%;" >
                                                                                <font size="3px" style="font-weight: 600 !important;"><?php echo $rr->date_created; ?></font>
                                                                                <br>
                                                                                <font size="2px" color="#7A7A7A">สร้างข้อมูล</font>
                                                                            </div>
                                                                            <div style="padding-top:13px;float:left;width:15%;" >
                                                                                <font id="price_vr_<?php echo $rr->product_category_id; ?>" size="3px" style="font-weight: 600 !important;"><?php echo $rr->date_modify; ?></font>
                                                                                <br>
                                                                                <font size="2px" color="#7A7A7A">อัพเดทล่าสุด</font>
                                                                            </div>
                                                                            <div style="float:left;width:15%;">
                                                                                <div align="right" style="padding-top:25px;padding-right:25px">
                                                                                    <a name="edit_variant" vr-id="<?php echo $rr->product_category_id; ?>"><i class="icon-pencil fa-lg"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                    <a name="del_variant" vr-id="<?php echo $rr->product_category_id; ?>"><i class="icon-trash fa-lg"></i></a>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div id="edit_vrbox_<?php echo $rr->product_category_id; ?>" name="edit_vrbox" style="width: 100%; color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;">
                                                                            <div style="padding: 25px">
                                                                                <div class="form-group">
                                                                                    <label>ประเภทสินค้า:</label>
                                                                                    <input id="edit_variant_name<?php echo $rr->product_category_id; ?>" vr-id="<?php echo $rr->product_category_id; ?>" type="text" class="form-control">
                                                                                </div>

                                                                                <div class="form-actions">
                                                                                    <button id="save_edit_variant<?php echo $rr->product_category_id; ?>" name="save_edit_variant" vr-id="<?php echo $rr->product_category_id; ?>" type="button" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                                                    <button id="cancle_edit_var<?php echo $rr->product_category_id; ?>" name="cancle_edit_var" vr-id="<?php echo $rr->product_category_id; ?>" type="button" class="btn btn-link">ยกเลิก</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div style="height:10px"></div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>

                                                            <hr>
                                                            <div id="hidden_box" class="">
                                                                <div class="">
                                                                    <h4 style="color:#444d58">เพิ่มประเภทสินค้าใหม่</h4>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>ประเภทสินค้า:</label>
                                                                    <input id="variant_name" name="product_name" type="text" class="form-control">
                                                                </div>
                                                                <div class="form-actions">
                                                                    <button id="save_variant" type="button" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                                    <button id="cancle_var" type="button" class="btn btn-link">ยกเลิก</button>
                                                                </div>
                                                            </div>
                                                            <div id="button_add_variant" align="center">
                                                                <button id="add_variant" type="button" style="height:40px;font-size: 20px;" class="btn btn-success"><i class="fa fa-plus"></i> เพิ่มตัวเลือกสินค้า</button>
                                                            </div>

                                                            <!--</div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--stock-->



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->

</div>

<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/pages/scripts/profile.min.js'); ?>" type="text/javascript"></script>


<script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/lib/markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/form-validation.min.js'); ?>" type="text/javascript"></script>

<link href="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/global/scripts/datatable.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/table-datatables-rowreorder.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/jquery.fine-uploader/jquery.fine-uploader.js'); ?>" type="text/javascript"></script>
<script>
    $(document).ready(function () {

        $("#hidden_box").hide();
        $("div[name='edit_vrbox']").hide();

        $("#add_variant").click(function () {
            $("#button_add_variant").hide();
            $("#hidden_box").slideToggle("slow");
        });

        $("#cancle_var").click(function () {
            $("#button_add_variant").show();
            $("#hidden_box").slideToggle("slow");
        });

        $("#save").click(function () {
            $("#button_add_variant").show();
        });

        //$("button[name='cancle_edit_var']").click(function () {
        $('body').on('click', "button[name='cancle_edit_var']", function (event) {
            var vr_id = $(this).attr("vr-id");
            $("#edit_vrbox_" + vr_id + "").hide();
            $("#in_vrbox" + vr_id + "").fadeIn();
        });

        var product_id = $("#hidden_idpd").val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('product/detail'); ?>",
            data: {
                product_id: product_id
            },
            dataType: "text",
            cache: false,
        }).done(function (data) {
            var json_obj = JSON.parse(data);
            console.log(json_obj);
            $.each(json_obj, function (key, detail) {

            });
        });

        $("#save").click(function () {
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            var detail = $("#inp_detail").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('product/update_detail/'); ?>",
                data: {
                    product_id: product_id,
                    product_name: product_name,
                    price: price,
                    detail: detail
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                if (data == 1) {
                    $("#product_name").val(product_name);
                    $("#price").val(price);
                    $('textarea#inp_detail').text(detail);
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });




        //$("a[name='edit_variant']").on( "click", function() {
        $('body').on('click', "a[name='edit_variant']", function (event) {
            var cr_id = $(this).attr("vr-id");
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage_product/some_category'); ?>",
                data: {
                    cr_id: cr_id
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                var json_obj = JSON.parse(data);
                console.log(json_obj);
                $.each(json_obj, function (key, detail) {
                    $("#edit_variant_name" + detail.product_category_id + "").val(detail.category);
                    $("#edit_vrbox_" + cr_id + "").slideDown();
                    $("#in_vrbox" + cr_id + "").hide();
                });
            });
        });


        $("#save_variant").click(function () {
            var store_id = $("#hidden_store_id").val();
            var variant_name = $("#variant_name").val();
            var date = new Date();
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var seconds = date.getSeconds();
            var date_now = year + "-0" + month + "-0" + day + " " + hours + ":" + minutes + ":" + seconds;

            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage_product/category_add/'); ?>",
                data: {
                    store_id: store_id,
                    variant_name: variant_name
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                //console.log(data);
                if (data != "") {
                    $("#variant_name").val("");
                    $("#out_vrbox").append('<div id="in_vrbox' + data + '" style="width: 100%; height: 70px;color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;"><div style="padding-top:13px;padding-left:25px;float:left;width:40%;" ><font id="name_vr_' + data + '" size="3px" style="font-weight: 600 !important;">' + variant_name + '</font><br><font size="2px" color="#7A7A7A">ประเภทสินค้า</font></div><div style="padding-top:13px;float:left;width:30%;" >\n\
            <font size="3px" style="font-weight: 600 !important;">' + date_now + '</font><br><font size="2px" color="#7A7A7A">สร้างข้อมูล</font>\n\
</div><div style="padding-top:13px;float:left;width:15%;" >\n\
<font size="3px" style="font-weight: 600 !important;">' + date_now + '</font><br><font size="2px" color="#7A7A7A">อัพเดทล่าสุด</font></div>\n\
<div style="float:left;width:15%;">\n\
<div align="right" style="padding-top:25px;padding-right:25px"><a name="edit_variant" vr-id="' + data + '">\n\
<i class="icon-pencil fa-lg"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a name="del_variant" vr-id="' + data + '">\n\
<i class="icon-trash fa-lg"></i></a></div></div></div>\n\
<div id="edit_vrbox_' + data + '" name="edit_vrbox" style="width: 100%; color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;">\n\
<div style="padding: 25px"><div class="form-group"><label>ตัวเลือกสินค้า:</label><input id="edit_variant_name' + data + '" type="text" class="form-control"></div>\n\
<div class="form-actions">\n\
<button id="save_edit_variant' + data + '" name="save_edit_variant" vr-id="' + data + '" type="button" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button><button id="cancle_edit_var' + data + '" name="cancle_edit_var" vr-id="' + data + '" type="button" class="btn btn-link">ยกเลิก</button></div></div></div><div style="height:10px"></div>');
                    $("div[name='edit_vrbox']").hide();
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });


        $('body').on('click', "a[name='del_variant']", function (event) {
            var vr_id = $(this).attr("vr-id");
            if (confirm("ยืนยันการลบ ?")) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('manage_product/category_del/'); ?>",
                    data: {
                        vr_id: vr_id
                    },
                    dataType: "text",
                    cache: false,
                }).done(function (data) {
                    //console.log(data);
                    if (data != "") {
                        $("#in_vrbox" + vr_id + "").remove();
                    } else {
                        alert("เกิดข้อผิดพลาด");
                    }
                });
            }
        });

        $('body').on('click', "button[name='save_edit_variant']", function (event) {
            var cr_id = $(this).attr("vr-id");
            var category = $("#edit_variant_name" + cr_id + "").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage_product/update_category'); ?>",
                data: {
                    cr_id: cr_id,
                    category: category
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                if (data == 1) {
                    $("#name_vr_" + cr_id + "").text(category);
                    $("#edit_vrbox_" + cr_id + "").hide();
                    $("#in_vrbox" + cr_id + "").fadeIn();
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });






    });
</script>
<?php $this->load->view('footer_2'); ?>