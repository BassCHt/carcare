<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/datatables/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
<style>
    a {
        text-decoration: none !important;
    }
</style>
<?php $this->load->view('header_2'); ?>

<?php $this->load->view('head_menu'); ?>

<?php
if (isset($_GET['status'])) {
    if ($_GET['status'] == 1) {
        echo "<input id='status' type='hidden' value='1'>";
    } else {
        echo "<input id='status' type='hidden' value='0'>";
    }
} else {
    $_GET['status'] = 0;
}
?>

<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>บริการ</h1>
                </div>
                <!-- END PAGE TITLE -->

                <!-- BEGIN PAGE TOOLBAR -->
                <div class="page-toolbar">
                    <!-- BEGIN THEME PANEL -->
                    <div class="btn-group btn-theme-panel">

                    </div>
                    <!-- END THEME PANEL -->
                </div>
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">

                <div id="notification_box">

                </div>

                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <a href="<?php echo base_url('service/add'); ?>">
                                        <button type="button" id="service_add" style="width:120px;padding-top:8px" class="btn green"><i class="fa fa-plus"></i> เพิ่มบริการ </button>
                                    </a>
                                    <div id="tools" class="tools"></div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="member_list_table">
                                        <thead>
                                            <tr>
                                                <th> บริการ </th>
                                                <th> ประเถท </th>
                                                <th> ราคา </th>
                                                <th> อัพเดทล่าสุด </th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_id">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->
</div>



<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/scripts/datatable.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/table-datatables-rowreorder.min.js'); ?>" type="text/javascript"></script>
<script>

    function hidden_box() {
        $(document).ready(function () {
            $("#notification_box").fadeOut("slow");
        });
    }
    setTimeout("hidden_box()", 2000);



    $(document).ready(function () {

        $("#box_service_add").hide();

        //$("#notification_box").append("<div class='note note-success'><span class='bold'><i class='fa fa-check' aria-hidden='true'></i> SUCCESS </span><span>&nbsp แก้ไขบริการเรียบร้อย</span> </div>");
        var status = $("#status").val();
        if (status == 1) {
            $("#notification_box").append("<div class='note note-success'><span class='bold'><i class='fa fa-check' aria-hidden='true'></i> SUCCESS </span><span>&nbsp แก้ไขบริการเรียบร้อย</span> </div>");
        } else if (status == 0) {
            $("#notification_box").append("<div class='note note-success'><span class='bold'></i> ERROR! </span><span>&nbsp Failed </span> </div>");
        }

        var table = $('#member_list_table');

        var oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            "sAjaxSource": "<?php echo site_url('service/service_list_ajax'); ?>",
            "bServerSide": true,
            "sServerMethod": "POST",
            "bSort": false,
            "ordering": false,
            'fnServerData': function (sSource, aoData, fnCallback) {

                $.ajax({
                    url: sSource,
                    type: 'POST',
                    cache: false,
                    data: aoData,
                    dataType: 'jsonp',
                    jsonp: 'jsoncallback',
                    success: fnCallback
                });
            },
            'fnDrawCallback': function (oSettings) {
                // alert('numtno');
            },
            // setup buttons extentension: http://datatables.net/extensions/buttons/
            buttons: [
                {extend: 'print', className: 'btn dark btn-outline'},
                {extend: 'pdf', className: 'btn green btn-outline'},
                {extend: 'csv', className: 'btn purple btn-outline '}
            ],
            "order": [
                [0, 'asc']
            ],
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        });


    });
</script>
<?php $this->load->view('footer_2'); ?>