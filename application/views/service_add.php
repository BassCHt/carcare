<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/jquery.fine-uploader/fine-uploader-new.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />

<?php $this->load->view('head_menu'); ?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>บริการ 
                        <small>เพิ่มบริการ</small>
                    </h1>
                </div>
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">

                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <div class="portlet-title tabbable-line">
                                                <div class="caption caption-md">
                                                    <i class="icon-globe theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase">รายละเอียดบริการ</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <form action="<?php echo base_url('service/service_add'); ?>" method="post">
                                                        <div class="form-group">
                                                            <label >บริการ</label>
                                                            <input type="text" class="form-control" name="inp_service" placeholder="">
                                                        </div>
                                                        <div id="sl_box" class="form-group"> 
                                                            <label class="control-label">ประเภท</label>   
                                                            <select id="sl_category" name="sl_category" class="form-control">
                                                                <?php foreach ($detail as $row) { ?>
                                                                <option value="<?php echo $row->service_category_id; ?>" class="custom-font"><?php echo $row->category; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>ราคา</label>
                                                            <input type="text" class="form-control" name="inp_price" placeholder="">
                                                        </div>
                                                        <center>
                                                            <button id="save_service" type="submit" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                            <a href="<?php echo base_url('service'); ?>">
                                                                <button id="cancle_save_service" type="button" class="btn btn-link">ยกเลิก</button>
                                                            </a>
                                                        </center>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->

</div>

<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/pages/scripts/profile.min.js'); ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/jquery.fine-uploader/jquery.fine-uploader.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/lib/markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/form-validation.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script>


    $(document).ready(function () {

        $("#save_edit").click(function () {
            var product_variant_id = $("#save_edit").attr("v_id");
            var price = $("#price").val();
            var variant = $("#variant").val();
            var detail = $('textarea#detail').val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('manage_product/update_variant/'); ?>",
                data: {
                    product_variant_id: product_variant_id,
                    variant: variant,
                    price: price,
                    detail: detail
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                if (data == 1) {
                    $("#variant").val(variant);
                    $("#price").val(price);
                    $('textarea#detail').text(detail);
                    window.location.href = "<?php echo base_url('manage_product?id=' . $row->product_id . '&status=1'); ?>";

                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });





    });

</script>
<?php $this->load->view('footer_2'); ?>