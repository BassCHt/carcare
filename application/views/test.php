<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script type="text/javascript" src="http://www.pureexample.com/js/lib/jquery.ui.touch-punch.min.js"></script>


<!-- CSS -->
<style type="text/css">
.list {
    background-color: #478BFF;
    font-size: 30px;
    text-align: center;
    cursor: pointer;
    font-family: Geneva,Arial,Helvetica,sans-serif;
    border: 1px solid gray;
}

#items .ui-selected {
    background: red;
    color: white;
    font-weight: bold;
}

#items {
    list-style-type: none;
    margin: 0;
    padding: 0;
}

#items li {
    float: left;
    margin: 2px;
    padding: 2px;
    width: 48px;
    height: 48px;
    line-height: 48px;
}

.highlight {
    border: 1px solid red;
    font-weight: bold;
    font-size: 45px;
    background-color: lightblue;
}
</style>

<!-- Javascript -->
<script>
$(function () {
	$("#items").sortable({
		placeholder: "highlight",
		start: function (event, ui) {
			ui.item.toggleClass("highlight");
		},
		stop: function (event, ui) {
			ui.item.toggleClass("highlight");
		}
	});
	$("#items").disableSelection();
});
</script>

<!-- HTML -->
<div style="width:520px">
  <ul id="items">
      <li class="list">1</li>
      <li class="list">2</li>
      <li class="list">3</li>
      <li class="list">4</li>
      <li class="list">5</li>
      <li class="list">6</li>
      <li class="list">7</li>
      <li class="list">8</li>
      <li class="list">9</li>
      <li class="list">10</li>
      <li class="list">11</li>
      <li class="list">12</li>
      <li class="list">13</li>
      <li class="list">14</li>
      <li class="list">15</li>
      <li class="list">16</li>
      <li class="list">17</li>
      <li class="list">18</li>
      <li class="list">19</li>
      <li class="list">20</li>
      <li class="list">21</li>
      <li class="list">22</li>
      <li class="list">23</li>
      <li class="list">24</li>
      <li class="list">25</li>
      <li class="list">26</li>
      <li class="list">27</li>
      <li class="list">28</li>
  </ul>
</div>
<div id="info" style="font-family:Geneva,Arial,Helvetica,sans-serif;font-size:20px;"></div>