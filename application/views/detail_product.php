<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />
<!--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />-->

<link href="<?php echo base_url('assets/global/plugins/datatables/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/jquery.fine-uploader/fine-uploader-new.css'); ?>" rel="stylesheet" type="text/css"/>
<script type="text/template" id="qq-template-manual-trigger">
    <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
    </div>
    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
    <span class="qq-upload-drop-area-text-selector"></span>
    </div>
    <div class="buttons">
    <div class="qq-upload-button-selector qq-upload-button" style="width:120px">
    <div>Select files</div>
    </div>
    <button type="button" id="trigger-upload" style="width:120px" class="btn btn-primary">
    <i class="icon-upload icon-white"></i> Upload
    </button>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
    <span>Processing dropped files...</span>
    <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
    <li>
    <div class="qq-progress-bar-container-selector">
    <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
    </div>
    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
    <span class="qq-upload-file-selector qq-upload-file"></span>
    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
    <span class="qq-upload-size-selector qq-upload-size"></span>
    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
    </li>
    </ul>

    <dialog class="qq-alert-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector">Close</button>
    </div>
    </dialog>

    <dialog class="qq-confirm-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector">No</button>
    <button type="button" class="qq-ok-button-selector">Yes</button>
    </div>
    </dialog>

    <dialog class="qq-prompt-dialog-selector">
    <div class="qq-dialog-message-selector"></div>
    <input type="text">
    <div class="qq-dialog-buttons">
    <button type="button" class="qq-cancel-button-selector">Cancel</button>
    <button type="button" class="qq-ok-button-selector">Ok</button>
    </div>
    </dialog>
    </div>
</script>

<style>

    a {
        text-decoration: none !important;
    }

    #trigger-upload {
        color: white;
        background-color: #00ABC7;
        font-size: 14px;
        padding: 7px 20px;
        background-image: none;
    }

    #fine-uploader-manual-trigger .qq-upload-button {
        margin-right: 15px;
    }

    #fine-uploader-manual-trigger .buttons {
        width: 36%;
    }

    #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
        width: 60%;
    }

    .img_box{
        width:120px;
        border-style: solid;
        border-color:#e1e5ec;
        border-width: 1px;
    }

</style>

<?php $this->load->view('head_menu'); ?>
<?php
foreach ($detail as $row) {
    $product_id = $row->product_id;
    $product_name = $row->product_name;
    $price = $row->price;
    $detail = $row->detail;
    $img = $row->img;
}
//
//        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//        $data = parse_url($url);
//        $idpd = basename($data['path'], '.html');
//        echo '<input type="hidden" id="hidden_iddd" value="'.$idpd.'">';
?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>รายละเอียดสินค้า</h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- END PAGE TOOLBAR -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <div id="notification_box">

                </div>
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">

                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <!--nav bar-->
                                            <ul id="tabs" class="nav nav-tabs nav-justified" data-tabs="tabs">
                                                <li class="active"><a href="#detail" data-toggle="tab">รายละเอียดสินค้า</a></li>
                                                <li><a href="#image" data-toggle="tab">รูปภาพ</a></li>
                                                <li><a href="#stock" data-toggle="tab">ตัวเลือกและ Stock สินค้า</a></li>
                                            </ul>
                                            <div id="my-tab-content" class="tab-content">
                                                <!--detail-->
                                                <div class="tab-pane active" id="detail"><br><br>
                                                    <div class="page-content-inner">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="portlet light ">
                                                                    <div class='form-group'>
                                                                        <!--<input id="hidden_idpd" type="hidden" value="<?php echo $product_id; ?>">-->
                                                                        <input type="hidden" id="hidden_idpd" value="<?php echo $product_id; ?>">
                                                                        <label class='control-label'>ชื่อสินค้า</label> 
                                                                        <input id='product_name' type='text' class='form-control' value="<?php echo $product_name; ?>"> 
                                                                    </div>
                                                                    <div class='form-group'>
                                                                        <label class='control-label'>ประเภทสินค้า</label>
                                                                        <div id="cr_option">
                                                                            <?php foreach ($category as $cr) { ?>
                                                                                <input id="checkbox<?php echo $cr->product_category_id; ?>" type="checkbox" name="checkbox[]"  value="<?php echo $cr->product_category_id; ?>"><?php echo $cr->category; ?><br>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class='form-group'>
                                                                        <label class='control-label'>ราคาตั้ง (฿)</label>
                                                                        <input id='price' type='text'  class='form-control' value="<?php echo $price; ?>"> 
                                                                    </div>
                                                                    <div class='form-group'>
                                                                        <label class='control-label'>รายละเอียด</label>
                                                                        <textarea id="inp_detail" rows="6" class='form-control'><?php echo $detail; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class='margiv-top-10'>
                                                                <center>
                                                                    <button type='button' id='save' p_id="<?php //echo $product_id    ?>" style="" class='btn green'> บันทึกรายการ </button>
                                                                    <a href="<?php echo base_url('product'); ?>">
                                                                        <button type='button' id='cancle' style="width:100px;"  class='btn btn-link'> Cancel </button>
                                                                    </a>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--detail-->

                                                <!--image-->
                                                <div class="tab-pane" id="image">
                                                    <br>
                                                    <div class="page-content-inner">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="portlet light ">
                                                                    <div id="fine-uploader-manual-trigger"></div>
                                                                </div>
                                                                <hr>
                                                                <?php if ($img != '' || $img != NULL) { ?>
                                                                    <div id="img_box" class="img_box">
                                                                        <center>
                                                                            <img src="<?php echo base_url('assets/img/upload/' . $img . ''); ?>" width="100px">
                                                                        </center>
                                                                        <button id="del_img" type="button" class="btn default" style="width:119px;">ลบ</button>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--image-->

                                                <!--stock-->
                                                <div class="tab-pane" id="stock">
                                                    <br>
                                                    <div class="page-content-inner">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div>
                                                                    <h4 style="color:#444d58">ตัวเลือกสินค้า</h4>
                                                                    <div id="out_vrbox">
                                                                        <?php foreach ($variants as $rr) { ?>

                                                                            <div id="in_vrbox<?php echo $rr->product_variant_id; ?>" style="width: 100%; height: 70px;color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;">
                                                                                <div style="padding-top:13px;padding-left:25px;float:left;width:40%;" >
                                                                                    <font id="name_vr_<?php echo $rr->product_variant_id; ?>" size="3px" style="font-weight: 600 !important;"><?php echo $rr->variant; ?></font>
                                                                                    <br>
                                                                                    <font size="2px" color="#7A7A7A">ตัวเลือกสินค้า</font>
                                                                                </div>
                                                                                <div style="padding-top:13px;float:left;width:30%;" >
                                                                                    <font size="3px" style="font-weight: 600 !important;">1241500620112</font>
                                                                                    <br>
                                                                                    <font size="2px" color="#7A7A7A">บาร์โค้ด</font>
                                                                                </div>
                                                                                <div style="padding-top:13px;float:left;width:15%;" >
                                                                                    <font id="price_vr_<?php echo $rr->product_variant_id; ?>" size="3px" style="font-weight: 600 !important;"><?php echo $rr->price; ?></font>
                                                                                    <br>
                                                                                    <font size="2px" color="#7A7A7A">ราคาขาย</font>
                                                                                </div>
                                                                                <div style="float:left;width:15%;">
                                                                                    <div align="right" style="padding-top:25px;padding-right:25px">
                                                                                        <a name="edit_variant" vr-id="<?php echo $rr->product_variant_id; ?>"><i class="icon-pencil fa-lg"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <a name="del_variant" vr-id="<?php echo $rr->product_variant_id; ?>"><i class="icon-trash fa-lg"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div id="edit_vrbox_<?php echo $rr->product_variant_id; ?>" name="edit_vrbox" style="width: 100%; color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;">
                                                                                <div style="padding: 25px">
                                                                                    <div class="form-group">
                                                                                        <label>ตัวเลือกสินค้า:</label>
                                                                                        <input id="edit_variant_name<?php echo $rr->product_variant_id; ?>" vr-id="<?php echo $rr->product_variant_id; ?>" type="text" class="form-control">
                                                                                    </div>
                                                                                    <div class="form-group">
                                                                                        <label>ราคา:</label>
                                                                                        <input id="edit_variant_price<?php echo $rr->product_variant_id; ?>" vr-id="<?php echo $rr->product_variant_id; ?>" type="text" class="form-control">
                                                                                    </div>
                                                                                    <div class="form-actions">
                                                                                        <button id="save_edit_variant<?php echo $rr->product_variant_id; ?>" name="save_edit_variant" vr-id="<?php echo $rr->product_variant_id; ?>" type="button" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                                                        <button id="cancle_edit_var<?php echo $rr->product_variant_id; ?>" name="cancle_edit_var" vr-id="<?php echo $rr->product_variant_id; ?>" type="button" class="btn btn-link">ยกเลิก</button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div style="height:10px"></div>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>

                                                                <hr>
                                                                <div id="hidden_box" class="">
                                                                    <div class="">
                                                                        <h4 style="color:#444d58">เพิ่มตัวเลือกสินค้าใหม่</h4>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>ตัวเลือกสินค้า:</label>
                                                                        <input id="variant_name" name="product_name" type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>ราคา:</label>
                                                                        <input id="variant_price" name="price" type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <button id="save_variant" type="button" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                                        <button id="cancle_var" type="button" class="btn btn-link">ยกเลิก</button>
                                                                    </div>
                                                                </div>
                                                                <div id="button_add_variant" align="center">
                                                                    <button id="add_variant" type="button" style="height:40px;font-size: 20px;" class="btn btn-success"><i class="fa fa-plus"></i> เพิ่มตัวเลือกสินค้า</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--nav bar-->                                   


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->

</div>

<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/pages/scripts/profile.min.js'); ?>" type="text/javascript"></script>


<script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/lib/markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/form-validation.min.js'); ?>" type="text/javascript"></script>

<link href="<?php echo base_url('assets/bootstrap/js/bootstrap.js'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/global/scripts/datatable.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/datatables.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/table-datatables-rowreorder.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/jquery.fine-uploader/jquery.fine-uploader.js'); ?>" type="text/javascript"></script>
<script>
    $(document).ready(function () {

        $("#hidden_box").hide();
        $("div[name='edit_vrbox']").hide();

        $("#add_variant").click(function () {
            $("#button_add_variant").hide();
            $("#hidden_box").slideToggle("slow");
        });

        $("#cancle_var").click(function () {
            $("#button_add_variant").show();
            $("#hidden_box").slideToggle("slow");
        });

        $("#save").click(function () {
            $("#button_add_variant").show();
        });

        //$("button[name='cancle_edit_var']").click(function () {
        $('body').on('click', "button[name='cancle_edit_var']", function (event) {
            var vr_id = $(this).attr("vr-id");
            $("#edit_vrbox_" + vr_id + "").hide();
            $("#in_vrbox" + vr_id + "").fadeIn();
        });

        var product_id = $("#hidden_idpd").val();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('manage_product/check_category'); ?>",
            data: {
                product_id: product_id
            },
            dataType: "text",
            cache: false,
        }).done(function (data) {
            var json_obj = JSON.parse(data);
            console.log(json_obj);
            $.each(json_obj, function (key, category) {
                $("#checkbox" + category.product_category_id + "").prop('checked', true);
            });
        });

        $("#save").click(function () {
            var product_name = $("#product_name").val();
            var price = $("#price").val();
            var detail = $("#inp_detail").val();
            var chk_val = [];
            $(':checkbox:checked').each(function (i) {
                chk_val[i] = $(this).val();
            });
            //console.log(chk_val);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('product/update_detail/'); ?>",
                data: {
                    product_id: product_id,
                    product_name: product_name,
                    price: price,
                    detail: detail,
                    chk_val: chk_val
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                //console.log(data);
                if (data == 1) {
                    $("#product_name").val(product_name);
                    $("#price").val(price);
                    $('textarea#inp_detail').text(detail);
                } else if (data == 2) {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });

        var manualUploader = new qq.FineUploader({
            element: document.getElementById('fine-uploader-manual-trigger'),
            template: 'qq-template-manual-trigger',
            request: {
                endpoint: '<?php echo base_url('upload/do_upload'); ?>',
                params: {'product_id': product_id}
            },
            thumbnails: {
                placeholders: {
                    waitingPath: '<?php echo base_url('assets/jquery.fine-uploader/placeholders/waiting-generic.png'); ?>',
                    notAvailablePath: '<?php echo base_url('assets/jquery.fine-uploader/placeholders/not_available-generic.png'); ?>'
                }
            },
            callbacks: {
                onComplete: function (id, fileName, responseJSON) {
                    if (responseJSON.success) {
                        //$("#m_img").remove();
                        //$("#member_img").append("<img id='m_img' src='<?php echo base_url(); ?>../assets/upload/" + responseJSON.filename + "' class='img-responsive' alt='' >");
                    }
                }
            },
            autoUpload: false,
            debug: true
        });

        qq(document.getElementById("trigger-upload")).attach("click", function () {
            manualUploader.uploadStoredFiles();
            //            var uploads = manualUploader.getUploads();
            //            console.log(uploads);

        });


        $("#del_img").click(function () {
            if (confirm("ยืนยันการลบรูปภาพ ?") == true) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('product/del_img/'); ?>",
                    data: {
                        product_id: product_id
                    },
                    dataType: "text",
                    cache: false,
                }).done(function (data) {
                    if (data == 1) {
                        $("#img_box").hide();
                    } else {
                        alert("เกิดข้อผิดพลาด");
                    }
                });
            } else {
            }
        });

        //$("a[name='edit_variant']").on( "click", function() {
        $('body').on('click', "a[name='edit_variant']", function (event) {
            var vr_id = $(this).attr("vr-id");
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('product/some_variant'); ?>",
                data: {
                    vr_id: vr_id
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                var json_obj = JSON.parse(data);
                console.log(json_obj);
                $.each(json_obj, function (key, detail) {
                    $("#edit_variant_name" + detail.product_variant_id + "").val(detail.variant);
                    $("#edit_variant_price" + detail.product_variant_id + "").val(detail.price);
                    $("#edit_vrbox_" + vr_id + "").slideDown();
                    $("#in_vrbox" + vr_id + "").hide();
                });
            });
        });


        $("#save_variant").click(function () {
            var variant_name = $("#variant_name").val();
            var variant_price = $("#variant_price").val();
            console.log(variant_name);
            console.log(variant_price);
            console.log(product_id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('product/variant_add/'); ?>",
                data: {
                    product_id: product_id,
                    variant_price: variant_price,
                    variant_name: variant_name
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                console.log(data);
                if (data != "") {
                    $("#variant_price").val("");
                    $("#variant_name").val("");
                    //$("#button_add_variant").show();
                    //$("#hidden_box").slideToggle("slow");
                    $("#out_vrbox").append('<div id="in_vrbox' + data + '" style="width: 100%; height: 70px;color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;"><div style="padding-top:13px;padding-left:25px;float:left;width:40%;" ><font id="name_vr_' + data + '" size="3px" style="font-weight: 600 !important;">' + variant_name + '</font><br><font size="2px" color="#7A7A7A">ตัวเลือกสินค้า</font></div><div style="padding-top:13px;float:left;width:30%;" ><font size="3px" style="font-weight: 600 !important;">1241500620112</font><br><font size="2px" color="#7A7A7A">บาร์โค้ด</font></div><div style="padding-top:13px;float:left;width:15%;" ><font id="price_vr_' + data + '" size="3px" style="font-weight: 600 !important;">' + variant_price + '</font><br><font size="2px" color="#7A7A7A">ราคาขาย</font></div><div style="float:left;width:15%;"><div align="right" style="padding-top:25px;padding-right:25px"><a name="edit_variant" vr-id="' + data + '"><i class="icon-pencil fa-lg"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a name="del_variant" vr-id="' + data + '"><i class="icon-trash fa-lg"></i></a></div></div></div><div id="edit_vrbox_' + data + '" name="edit_vrbox" style="width: 100%; color:#444d58; background-color: #ddd; border: 1px solid #444d58; border-radius: 5px !important;"><div style="padding: 25px"><div class="form-group"><label>ตัวเลือกสินค้า:</label><input id="edit_variant_name' + data + '" type="text" class="form-control"></div><div class="form-group"><label>ราคา:</label><input id="edit_variant_price' + data + '" type="text" class="form-control"></div><div class="form-actions"><button id="save_edit_variant' + data + '" name="save_edit_variant" vr-id="' + data + '" type="button" class="btn btn-success btn-large" style="">บันทึกรายการ</button><button id="cancle_edit_var' + data + '" name="cancle_edit_var" vr-id="' + data + '" type="button" class="btn btn-link">ยกเลิก</button></div></div></div><div style="height:10px"></div>');
                    $("div[name='edit_vrbox']").hide();
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });


        $('body').on('click', "a[name='del_variant']", function (event) {
            var vr_id = $(this).attr("vr-id");
            if (confirm("ยืนยันการลบ ?")) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('product/variant_del/'); ?>",
                    data: {
                        vr_id: vr_id
                    },
                    dataType: "text",
                    cache: false,
                }).done(function (data) {
                    //console.log(data);
                    if (data != "") {
                        $("#in_vrbox" + vr_id + "").remove();
                    } else {
                        alert("เกิดข้อผิดพลาด");
                    }
                });
            }
        });

        $('body').on('click', "button[name='save_edit_variant']", function (event) {
            var vr_id = $(this).attr("vr-id");
            var variant = $("#edit_variant_name" + vr_id + "").val();
            var price = $("#edit_variant_price" + vr_id + "").val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('product/update_variant'); ?>",
                data: {
                    vr_id: vr_id,
                    variant: variant,
                    price: price
                },
                dataType: "text",
                cache: false,
            }).done(function (data) {
                if (data == 1) {
                    $("#name_vr_" + vr_id + "").text(variant);
                    $("#price_vr_" + vr_id + "").text(price);
                    $("#edit_vrbox_" + vr_id + "").hide();
                    $("#in_vrbox" + vr_id + "").fadeIn();
                } else {
                    alert("เกิดข้อผิดพลาด");
                }
            });
        });






    });
</script>
<?php $this->load->view('footer_2'); ?>