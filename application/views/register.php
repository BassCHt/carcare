<?php $this->load->view('header_1'); ?>
<link href="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/pages/css/profile.min.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo base_url('assets/jquery.fine-uploader/fine-uploader-new.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css'); ?>" rel="stylesheet" type="text/css" />

<?php $this->load->view('head_menu'); ?>

<div class="page-container">
    <!-- BEGIN CONTENT -->

    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>สมัครสมาชิก 

                    </h1>
                </div>
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">

                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="profile-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light ">
                                            <!--                                            <div class="portlet-title tabbable-line">
                                                                                            <div class="caption caption-md">
                                                                                                <i class="icon-globe theme-font hide"></i>
                                                                                                <span class="caption-subject font-blue-madison bold uppercase">รายละเอียดบริการ</span>
                                                                                            </div>
                                                                                        </div>-->
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <div class="form-group">
                                                        <label >ชื่อผู้ใช้งาน</label>
                                                        <input type="text" class="form-control" id="username" placeholder="ชื่อผู้ใช้งาน">
                                                    </div>
                                                    <div id="sl_box" class="form-group"> 
                                                        <label class="control-label">อีเมล์</label>   
                                                        <input type="text" class="form-control" id="email" placeholder="อีเมล์">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>รหัสผ่าน</label>
                                                        <input type="password" class="form-control" id="password" placeholder="รหัสผ่าน">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>ยืนยันรหัสผ่าน</label>
                                                        <input type="password" class="form-control" id="password2" placeholder="ยืนยันรหัสผ่าน">
                                                    </div>
                                                    <center>
                                                        <button id="register" type="button" class="btn btn-success btn-large" style="width:120px;">บันทึกรายการ</button>
                                                        <a href="<?php echo base_url(); ?>">
                                                            <button id="cancle_save_service" type="button" class="btn btn-link">ยกเลิก</button>
                                                        </a>
                                                    </center>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <!-- BEGIN QUICK SIDEBAR -->
    <a href="javascript:;" class="page-quick-sidebar-toggler">
        <i class="icon-login"></i>
    </a>

    <!-- END QUICK SIDEBAR -->

</div>

<?php $this->load->view('footer_1'); ?>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery.sparkline.min.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/pages/scripts/profile.min.js'); ?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/jquery.fine-uploader/jquery.fine-uploader.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/select2/js/select2.full.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/jquery-validation/js/additional-methods.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript'); ?>"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/lib/markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/pages/scripts/form-validation.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script>


    $(document).ready(function () {

        $("#register").click(function () {
            var username = $("#username").val();
            var password = $("#password").val();
            var password2 = $("#password2").val();
            var email = $('#email').val();
            if (password == password2) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('register/member_add/'); ?>",
                    data: {
                        username: username,
                        password: password,
                        email: email
                    },
                    dataType: "text",
                    cache: false,
                }).done(function (data) {
                    if (data == 1) {
                        //window.location.href = '<?php echo base_url('login'); ?>';
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url('login/check_login'); ?>",
                            data: {
                                username: username, 
                                password: password
                            },
                            dataType: "text",
                            cache: false,
                        }).done(function (data) {
                            if (data == 1) {
                                window.location.href = '<?php echo base_url('store'); ?>';
                            } else {
                                alert("เกิดข้อผิดพลาด");
                            }
                        });
                    } else {
                        alert("เกิดข้อผิดพลาด");
                    }
                });
            } else {
                alert("รหัสผ่านไม่ตรงกัน");
            }
        });





    });

</script>
<?php $this->load->view('footer_2'); ?>