<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <div class="page-logo">

                <img src="<?php echo base_url('assets/img/logo01.png'); ?>" width="270" height="50" alt="logo" class="logo-img">

            </div>
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?php echo base_url('assets/img/admin.png') ?>">
                            <span class="username username-hide-mobile"><?php echo $this->session->userdata("username"); ?></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-gear"></i> Change Password 
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('login/logout') ?>">
                                    <i class="icon-key"></i> Log Out 
                                </a>
                            </li>
                        </ul>
                </ul>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="<?php echo base_url('login/change_password'); ?>" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Change password</h4>
                    </div>
                    <div class="modal-body">
                        <div class='form-group'>
                            <label class='control-label'>Old password</label> 
                            <input name='old_pass' type='password' class='form-control'> 
                        </div>
                        <div class='form-group'>
                            <label class='control-label'>New password</label> 
                            <input name='new_pass' type='password' class='form-control'> 
                        </div>
                        <div class='form-group'>
                            <label class='control-label'>Confirm password</label> 
                            <input name='new_pass2' type='password' class='form-control'> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <input type="submit" class="btn green" style="width:200px;" value="OK"><br>
                        </center>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN HEADER SEARCH BOX -->
            <form class="search-form" action="page_general_search.html" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="query">
                    <span class="input-group-btn">
                        <a href="javascript:;" class="btn submit">
                            <i class="icon-magnifier"></i>
                        </a>
                    </span>
                </div>
            </form>
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu  ">
                <ul class="nav navbar-nav">
                    <li class="menu-dropdown classic-menu-dropdown <?php echo ($this->main_nav == 'dashboard' ? 'active' : '') ?> ">
                        <a href="<?php echo base_url('dashboard'); ?>">Dashboard
                            <span class="arrow"></span>
                        </a>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown <?php echo ($this->main_nav == 'product' ? 'active' : '') ?>">
                        <a href="<?php echo base_url('product'); ?>">สินค้า
                            <span class="arrow"></span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class="<?php echo ($this->main_nav == 'product' ? 'active' : '') ?>">
                                <a href="<?php echo base_url('product'); ?>" class="nav-link  ">จัดการสินค้า</a>
                            </li>
                            <li class="<?php echo ($this->main_nav == 'product' ? 'active' : '') ?>">
                                <a href="<?php echo base_url('product/category'); ?>" class="nav-link  ">ประเภทสินค้า</a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu-dropdown classic-menu-dropdown <?php echo ($this->main_nav == 'service' ? 'active' : '') ?>">
                        <a href="<?php echo base_url('service'); ?>">บริการ
                            <span class="arrow"></span>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class="<?php echo ($this->main_nav == 'service' ? 'active' : '') ?>">
                                <a href="<?php echo base_url('service'); ?>" class="nav-link  ">จัดการบริการ</a>
                            </li>
                            <li class="<?php echo ($this->main_nav == 'service' ? 'active' : '') ?>">
                                <a href="<?php echo base_url('service/category'); ?>" class="nav-link  ">ประเภทบริการ</a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="menu-dropdown classic-menu-dropdown <?php echo ($this->main_nav == 'register' ? 'active' : '') ?> ">
                        <a href="<?php echo base_url('register'); ?>">สมัครสมาชิก
                            <span class="arrow"></span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>

