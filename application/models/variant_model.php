<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Variant_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function variant_add($ar){
        if($this->db->insert('service_variant',$ar)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
