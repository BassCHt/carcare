<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function save_member($ar){
        if($this->db->insert('user',$ar)){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
