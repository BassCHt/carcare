<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_price($variant, $product_id = "", $name = "") {
        if ($product_id == "" && $name == "") {
            $this->db->select("price")->where("variant", $variant);
            $query = $this->db->get("product_variant");
            $row = $query->row();
            return $row->price;
        } else if ($product_id != "" && $name == "") {
            $this->db->select("price")->where("product_id", $product_id)->where("variant", $variant);
            $query = $this->db->get("product_variant");
            $row = $query->row();
            return $row->price;
        } else if ($name != "") {
            $this->db->select("price")->where("product_name", $name);
            $query = $this->db->get("product");
            $row = $query->row();
            return $row->price;
        }
    }

    function load_service($sevice_id) {
        $this->db->select('*')
                ->where("service_id", $sevice_id);
        $query = $this->db->get('service');
        return $query->result();
    }

    function load_variant($variant_id) {
        $this->db->select('*')
                ->where("service_variant_id", $variant_id);
        $query = $this->db->get('service_variant');
        return $query->result();
    }

    function update_service($service_id, $ar) {
        if ($this->db->where("service_id", $service_id)->update("service", $ar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_variant($service_variant_id, $ar) {
        $check = $this->db->where("service_variant_id", $service_variant_id)->update("service_variant", $ar);
        if ($check) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_id($name) {
        $this->db->select("product_id")->where("product_name", $name);
        $query = $this->db->get("product");
        $row = $query->row();
        return $row->product_id;
    }

    function product_detail($product_id, $variant) {
        $this->db->select('*')
                ->where("product_variant.product_id", $product_id)
                ->where("product_variant.variant", $variant)
                ->join('product', 'product.product_id = product_variant.product_id');
        $query = $this->db->get('product_variant');
        return $query->result();
    }
    
    function service_add($ar){
        if($this->db->insert('service',$ar)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    function category_add($ar){
        if($this->db->insert('service_category',$ar)){
            return TRUE;
        }else{
            return FALSE;
        }
    }
            
    function get_category(){
        $this->db->select('service_category_id,category');
        $query = $this->db->get('service_category');
        return $query->result();
    }
    
    function some_category($id){
        $this->db->select('category,service_category_id')->where('service_category_id',$id);
        $query = $this->db->get('service_category');
        return $query->result();
    }
    
    function update_category($id,$ar){
        if ($this->db->where("service_category_id", $id)->update("service_category", $ar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
