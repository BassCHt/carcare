<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insert_product($ar) {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');

        $this->db->select("store_id")
                ->where("user_id", $this->session->userdata("user_id"));
        $query = $this->db->get("store");
        foreach ($query->result() as $row) {
            $id = $row->store_id;
        }
        $this->db->select("product_category_id")
                ->where("store_id", $id)
                ->where("category", 'สินค้าทั้งหมด');
        $query2 = $this->db->get("product_category");
        if ($query2->result()) {
            $this->db->insert('product', $ar);
            $insert_pd_id = $this->db->insert_id();

            $this->db->select("price")
                    ->where("product_id", $insert_pd_id);
            $query_price = $this->db->get("product");
            foreach ($query_price->result() as $row_price) {
                $price = $row_price->price;
            }

            $ar_vr = array(
                'date_created' => $date_now,
                'date_modify' => $date_now,
                'product_id' => $insert_pd_id,
                'variant' => 'ธรรมดา',
                'price' => $price,
                'status' => 1
            );
            $this->db->insert('product_variant', $ar_vr);

            return $insert_pd_id;
        } else {
            $arr = array(
                'date_created' => $date_now,
                'date_modify' => $date_now,
                'category' => 'สินค้าทั้งหมด',
                'store_id' => $id
            );
            $this->db->insert('product_category', $arr);
            $insert_cr_id = $this->db->insert_id();

            $this->db->insert('product', $ar);
            $insert_pd_id = $this->db->insert_id();

            $this->db->select("price")
                    ->where("product_id", $insert_pd_id);
            $query_price = $this->db->get("product");
            foreach ($query_price->result() as $row_price) {
                $price = $row_price->price;
            }
            $ar_vr = array(
                'date_created' => $date_now,
                'date_modify' => $date_now,
                'product_id' => $insert_pd_id,
                'variant' => 'ธรรมดา',
                'price' => $price,
                'status' => 1
            );
            $this->db->insert('product_variant', $ar_vr);

            $ar_match = array(
                'date_created' => $date_now,
                'date_modify' => $date_now,
                'product_id' => $insert_pd_id,
                'product_category_id' => $insert_cr_id
            );
            $this->db->insert('product_match', $ar_match);

            return $insert_pd_id;
        }
    }

    function update_detail($idpd, $ar) {
        if ($this->db->where("product_id", $idpd)->update("product", $ar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_product($idpd) {
        $this->db->select("*")
                ->where("product_id", $idpd);
        $query = $this->db->get("product");
        return $query->result();
    }

    function get_variant($idpd) {
        $this->db->select("*")
                ->where("product_id", $idpd);
        $query = $this->db->get("product_variant");
        return $query->result();
    }

    function some_variant($vr_id) {
        $this->db->select("*")
                ->where("product_variant_id", $vr_id);
        $query = $this->db->get("product_variant");
        return $query->result();
    }

    function all_product() {
        $this->db->select("*")
                ->where("user_id", $this->session->userdata("user_id"));
        $query = $this->db->get("product");
        return $query->result();
    }

    function img_update($product_id, $data) {
        if ($this->db->where("product_id", $product_id)->update("product", $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function variant_add($ar) {
        $this->db->insert('product_variant', $ar);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            return $insert_id;
        } else {
            return FALSE;
        }
    }

    function variant_del($vr_id) {
        $this->db->where('product_variant_id', $vr_id);
        if ($this->db->delete('product_variant')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function update_variant($vr_id, $ar) {
        if ($this->db->where("product_variant_id", $vr_id)->update("product_variant", $ar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function get_product_category() {
        $this->db->select("*");
        $query = $this->db->get("product_category");
        return $query->result();
    }

    function some_category($cr_id) {
        $this->db->select("*")
                ->where("product_category_id", $cr_id);
        $query = $this->db->get("product_category");
        return $query->result();
    }

    function check_category($product_id) {
        $this->db->select('product_category_id')
                ->where("product_id", $product_id);
        //->join('product', 'product.product_id = product_category.product_id');
        $query = $this->db->get('product_match');
        return $query->result();
    }

    function update_category($cr_id, $ar) {
        if ($this->db->where("product_category_id", $cr_id)->update("product_category", $ar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function category_add($ar) {
        $this->db->insert('product_category', $ar);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            return $insert_id;
        } else {
            return FALSE;
        }
    }

    function category_del($vr_id) {
        $this->db->where('product_category_id', $vr_id);
        if ($this->db->delete('product_category')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function save_product_match($product_match_ar) {
        if ($this->db->insert('product_match', $product_match_ar)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_duplicate($product_id, $checkbox_val) {
        $this->db->select('product_category_id')
                ->where("product_id", $product_id)
                ->where("product_category_id", $checkbox_val);
        $query = $this->db->get('product_match');
        $row = $query->row();
        return $row->product_category_id;
    }

}

?>