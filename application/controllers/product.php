<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'product';
        $this->load->model('product_model');
    }

    public function index() {
        $data['detail'] = $this->product_model->all_product();
        //$data['history'] = $this->db_model->load_history();
        $this->load->view('product', $data);
    }

    public function add() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $product_name = $this->input->post('product_name');
        $price = $this->input->post('price');
        $ar = array(
            'date_created' => $date_now,
            'date_modify' => $date_now,
            'sort_id' => NULL,
            'product_name' => $product_name,
            'user_id' => $this->session->userdata("user_id"),
            'detail' => '',
            'price' => $price,
            'status' => 1
        );
        $pd_id = $this->product_model->insert_product($ar);
        echo ("<SCRIPT LANGUAGE='JavaScript'>
        window.location.href='" . base_url('product/detail/' . $pd_id . '') . "';
        </SCRIPT>");
    }

    public function detail() {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $data = parse_url($url);
        $idpd = basename($data['path'], '.html');
        $data['detail'] = $this->product_model->get_product($idpd);
        $data['variants'] = $this->product_model->get_variant($idpd);
        $data['category'] = $this->product_model->get_product_category();
        $this->load->view('detail_product', $data);
    }

    function update_detail() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $product_id = $this->input->post("product_id");

        $checkbox_val_ar = $this->input->post("chk_val");
        $arrlength = count($checkbox_val_ar);
        for ($x = 0; $x < $arrlength; $x++) {
            $duplicate = $this->product_model->check_duplicate($product_id, $checkbox_val_ar[$x]);
            if ($duplicate == "" || $duplicate == NULL) {
                $product_match_ar = array(
                    "date_created" => $date_now,
                    "date_modify" => $date_now,
                    "product_id" => $product_id,
                    "product_category_id" => $checkbox_val_ar[$x]
                );
                $this->product_model->save_product_match($product_match_ar);
            } else {
                //return TRUE;
            }
        }
        $ar = array(
            "date_modify" => $date_now,
            "product_name" => $this->input->post("product_name"),
            "price" => $this->input->post("price"),
            "detail" => $this->input->post("detail")
        );
        if ($this->product_model->update_detail($product_id, $ar)) {
            echo '1';
        } else {
            echo '2';
        }
        //echo $this->input->post("product_id")."/".$this->input->post("product_name").'/'.$this->input->post("price").'/'.$this->input->post("detail");
    }

    function del_img() {
        $product_id = $this->input->post("product_id");
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_modify" => $date_now,
            "img" => ""
        );
        if ($this->product_model->update_detail($product_id, $ar)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    function variant_del() {
        $vr_id = $this->input->post("vr_id");
        if ($this->product_model->variant_del($vr_id)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    function variant_add() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_created" => $date_now,
            "date_modify" => $date_now,
            "variant" => $this->input->post("variant_name"),
            "price" => $this->input->post("variant_price"),
            "product_id" => $this->input->post("product_id"),
            "status" => 1
        );
        //echo $this->input->post("variant_name")."/".$this->input->post("variant_price")."/".$this->input->post("product_id"); 
        if ($last_id = $this->product_model->variant_add($ar)) {
            echo $last_id;
        } else {
            echo '2';
        }
    }

    function active() {
        $product_id = $this->input->post("product_id");
        $state = $this->input->post("state");
        if ($state == 'true') {
            $status = '1';
        } else if ($state == 'false') {
            $status = '0';
        }
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_modify" => $date_now,
            "status" => $status
        );
        if ($this->product_model->update_detail($product_id, $ar)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    function some_variant() {
        $vr_id = $this->input->post("vr_id");
        $data = $this->product_model->some_variant($vr_id);
        echo json_encode($data);
    }

    function update_variant() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $vr_id = $this->input->post("vr_id");
        $ar = array(
            "date_modify" => $date_now,
            "variant" => $this->input->post("variant"),
            "price" => $this->input->post("price")
        );
        if ($this->product_model->update_variant($vr_id, $ar)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function category() {
        $data['category'] = $this->product_model->get_product_category();
        $this->load->view('product_category', $data);
    }

}
