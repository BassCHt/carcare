<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service_variant extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'service';
        $this->load->model('variant_model');
    }

    public function index() {
        
    }

    
    public function add(){
        $this->load->view('variant_add');
    }
    
    public function variant_add(){
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_created" => $date_now,
            "date_modify" => $date_now,
            "service_id" => $this->input->post("hidden_svid"),
            "variant" => $this->input->post("inp_variant"),
            "price" => $this->input->post("inp_price"),
            "detail" => $this->input->post("inp_detail")
        );
        if($this->variant_model->variant_add($ar)){
            redirect(base_url('manage_service?id='.$this->input->post("hidden_svid").''));
        }else{
            alert('เกิดข้อผิดพลาด');
        }
    }

}
