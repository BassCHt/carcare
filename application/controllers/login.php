<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("db_model");
    }

    public function index() {
        $this->load->view('login');
    }

    function encode_md5() {
        $str = "123456";
        echo md5($str);
    }

    public function check_login() {
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $check = $this->db_model->check_login($username, $password);
        if ($check) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url('login'));
    }

//    public function change_password() {
//        $old_pass = $this->input->post("old_pass");
//        $new_pass = $this->input->post("new_pass");
//        $new_pass2 = $this->input->post("new_pass2");
//
//        if ($new_pass != $new_pass2) {
//            echo ("<SCRIPT LANGUAGE='JavaScript'>
//                window.alert('Passwords do not match')
//                window.history.back();
//                </SCRIPT>");
//        } else {
//            $chk = $this->db_model->update_password($old_pass, $new_pass);
//            if ($chk) {
//                $this->session->sess_destroy();
//                echo ("<SCRIPT LANGUAGE='JavaScript'>
//                    window.alert('Password changed')
//                    window.location.href='".base_url('login')."';
//                    </SCRIPT>");
//            } else {
//                echo ("<SCRIPT LANGUAGE='JavaScript'>
//                window.alert('Old password is incorrect')
//                window.history.back();
//                </SCRIPT>");
//            }
//        }
//    }
}
