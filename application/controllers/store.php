<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Store extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'register';
        $this->load->model('store_model');
    }

    public function index() {
        $this->load->view('store_add');
    }
    
    public function add(){
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            'date_created' => $date_now,
            'date_modify' => $date_now,
            'user_id' => $this->session->userdata("user_id"),
            'store_name' => $this->input->post('store_name'),
            'address' => $this->input->post('address'),
            'email' => $this->input->post('email'),
            'tel' => $this->input->post('tel')
        );
        //echo $this->session->userdata("user_id")."/".$this->input->post('store_name')."/".$this->input->post('address')."/".$this->input->post('email')."/".$this->input->post('tel');s
        if($this->store_model->save($ar)){
            echo '1';
        }else{
            echo '2';
        }
    }

} 