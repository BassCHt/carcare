<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Service extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'service';
        $this->load->model('service_model');
    }

    public function index() {
        //$data['detail'] = $this->db_model->quotation();
        //$data['history'] = $this->db_model->load_history();
        $this->load->view('service');
    }

    function service_list_ajax() {
        if ($this->input->is_ajax_request() AND $this->input->server('REQUEST_METHOD') == 'POST') {

            // Field Parameter
            $tb = 'service s';
            $column_index = 's.service_id';
            $column_select = 's.service_id,s.service,s.price,s.category,s.date_modify';

            $join = array();

            $init_where = 'service_id != 0';

            $column_search = array('s.service', 's.category', 's.price');
            $column_search_num = count($column_search);

            $column_sort = array('s.service_id');

            $join_type = array();
            // Filter all column (WHERE)
            $where = $init_where;
            if ($this->input->post('sSearch') !== FALSE and $this->input->post('sSearch') != '') {
                $search = $this->input->post('sSearch');
                $where .= ((!is_null($where) and $where != '') ? ' AND (' : '(');
                for ($c = 0; $c < $column_search_num; $c++) {
                    if ($c == 0) {
                        $where .= $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str($search) . '%"';
                    } else {
                        $where .= ' OR ' . $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str($search) . '%"';
                    }
                }
                $where .= ')';
            }

            // Filter each column (WHERE)
            for ($c = 0; $c < $column_search_num; $c++) {
                if ($this->input->post('bSearchable_' . $c) !== FALSE and $this->input->post('sSearch_' . $c) !== FALSE and $this->input->post('sSearch_' . $c) != '') {
                    ${'search_' . $c} = $this->input->post('sSearch_' . $c);
                    $where .= ' AND ' . $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str(${'search_' . $c}) . '%"';
                }
            }

            // Order By
            $order_by = NULL;
            if ($this->input->post('iSortCol_0') !== FALSE) {
                $iSortingCols = (int) $this->input->post('iSortingCols');
                for ($s = 0; $s < $iSortingCols; $s++) {
                    if ($s == 0) {
                        $order_by .= $column_sort[$this->input->post('iSortCol_' . $s)] . ' ' . strtoupper($this->input->post('sSortDir_' . $s));
                    } else {
                        $order_by .= ', ' . $column_sort[$this->input->post('iSortCol_' . $s)] . ' ' . strtoupper($this->input->post('sSortDir_' . $s));
                    }
                }
            }

            // Limit
            if ($this->input->post('iDisplayLength') != -1) {
                $limit = array($this->input->post('iDisplayLength'), $this->input->post('iDisplayStart'));
            } else {
                $limit = NULL;
            }

            // iTotalRecords
            $query_all_row = $this->db_model->getAllJoin($tb, $column_index, NULL, $init_where, NULL, NULL, NULL);
            $all_row = $query_all_row->num_rows();

            // Filter Data
            $this->db->select('SQL_CALC_FOUND_ROWS ' . $column_select, FALSE);
            $service_query = $this->db_model->getAllJoin($tb, NULL, NULL, $where, $order_by, $limit, NULL);

            // iTotalDisplayRecords
            $query_found_row = $this->db_model->getQuery('SELECT FOUND_ROWS() AS "found_row"');
            $found_row = $query_found_row->row()->found_row;

            // Output
            $data = array(
                'sEcho' => (int) $this->input->post('sEcho'),
                'iTotalRecords' => $all_row,
                'iTotalDisplayRecords' => $found_row,
                //'query' => $this->db->last_query(),
                'aaData' => array()
            );
            if ($service_query->num_rows() > 0) {
                foreach ($service_query->result() as $service) {
                    $row = array(
                        '0' => "<a href=" . base_url('manage_service?id=' . $service->service_id . ' ') . ">" . $service->service . "</a>",
                        '1' => $service->category,
                        '2' => $service->price,
                        '3' => $service->date_modify
                    );
                    $data['aaData'][] = $row;
                }
            }
            echo $this->input->get('jsoncallback') . '(' . json_encode($data) . ')';
        } else {
            exit();
        }
    }

    function service_category_ajax() {
        if ($this->input->is_ajax_request() AND $this->input->server('REQUEST_METHOD') == 'POST') {

            // Field Parameter
            $tb = 'service_category s';
            $column_index = 's.service_category_id';
            $column_select = 's.service_category_id,s.category,s.date_created,s.date_modify';

            $join = array();

            $init_where = 'service_category_id != 0';

            $column_search = array('s.category');
            $column_search_num = count($column_search);

            $column_sort = array('s.service_category_id');

            $join_type = array();
            // Filter all column (WHERE)
            $where = $init_where;
            if ($this->input->post('sSearch') !== FALSE and $this->input->post('sSearch') != '') {
                $search = $this->input->post('sSearch');
                $where .= ((!is_null($where) and $where != '') ? ' AND (' : '(');
                for ($c = 0; $c < $column_search_num; $c++) {
                    if ($c == 0) {
                        $where .= $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str($search) . '%"';
                    } else {
                        $where .= ' OR ' . $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str($search) . '%"';
                    }
                }
                $where .= ')';
            }

            // Filter each column (WHERE)
            for ($c = 0; $c < $column_search_num; $c++) {
                if ($this->input->post('bSearchable_' . $c) !== FALSE and $this->input->post('sSearch_' . $c) !== FALSE and $this->input->post('sSearch_' . $c) != '') {
                    ${'search_' . $c} = $this->input->post('sSearch_' . $c);
                    $where .= ' AND ' . $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str(${'search_' . $c}) . '%"';
                }
            }

            // Order By
            $order_by = NULL;
            if ($this->input->post('iSortCol_0') !== FALSE) {
                $iSortingCols = (int) $this->input->post('iSortingCols');
                for ($s = 0; $s < $iSortingCols; $s++) {
                    if ($s == 0) {
                        $order_by .= $column_sort[$this->input->post('iSortCol_' . $s)] . ' ' . strtoupper($this->input->post('sSortDir_' . $s));
                    } else {
                        $order_by .= ', ' . $column_sort[$this->input->post('iSortCol_' . $s)] . ' ' . strtoupper($this->input->post('sSortDir_' . $s));
                    }
                }
            }

            // Limit
            if ($this->input->post('iDisplayLength') != -1) {
                $limit = array($this->input->post('iDisplayLength'), $this->input->post('iDisplayStart'));
            } else {
                $limit = NULL;
            }

            // iTotalRecords
            $query_all_row = $this->db_model->getAllJoin($tb, $column_index, NULL, $init_where, NULL, NULL, NULL);
            $all_row = $query_all_row->num_rows();

            // Filter Data
            $this->db->select('SQL_CALC_FOUND_ROWS ' . $column_select, FALSE);
            $category_query = $this->db_model->getAllJoin($tb, NULL, NULL, $where, $order_by, $limit, NULL);

            // iTotalDisplayRecords
            $query_found_row = $this->db_model->getQuery('SELECT FOUND_ROWS() AS "found_row"');
            $found_row = $query_found_row->row()->found_row;

            // Output
            $data = array(
                'sEcho' => (int) $this->input->post('sEcho'),
                'iTotalRecords' => $all_row,
                'iTotalDisplayRecords' => $found_row,
                //'query' => $this->db->last_query(),
                'aaData' => array()
            );
            if ($category_query->num_rows() > 0) {
                foreach ($category_query->result() as $category) {
                    $row = array(
                        '0' => '<a href="'.base_url('service/edit_service_category?id='.$category->service_category_id.'').'">' . $category->category . '</a>',
                        '1' => $category->date_created,
                        '2' => $category->date_modify
                    );
                    $data['aaData'][] = $row;
//                    $data['for_modal'][] = array(
//                        'category_name' => $category->category,
//                        'category_id' => $category->service_category_id
//                    );
                }
            }
            echo $this->input->get('jsoncallback') . '(' . json_encode($data) . ')';
        } else {
            exit();
        }
    }

    function variant_list_ajax() {

        $service_id = $this->input->get("id");

        if ($this->input->is_ajax_request() AND $this->input->server('REQUEST_METHOD') == 'POST') {

            // Field Parameter
            $tb = 'service_variant p';
            $column_index = 'p.service_variant_id';
            $column_select = 'p.service_variant_id,p.service_id,p.variant,p.price,p.detail';

            $join = array();

            $init_where = 'service_id = ' . $service_id . '';

            $column_search = array('p.variant', 'p.price', 'p.detail');
            $column_search_num = count($column_search);

            $column_sort = array('p.service_variant_id');

            $join_type = array();
            // Filter all column (WHERE)
            $where = $init_where;
            if ($this->input->post('sSearch') !== FALSE and $this->input->post('sSearch') != '') {
                $search = $this->input->post('sSearch');
                $where .= ((!is_null($where) and $where != '') ? ' AND (' : '(');
                for ($c = 0; $c < $column_search_num; $c++) {
                    if ($c == 0) {
                        $where .= $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str($search) . '%"';
                    } else {
                        $where .= ' OR ' . $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str($search) . '%"';
                    }
                }
                $where .= ')';
            }

            // Filter each column (WHERE)
            for ($c = 0; $c < $column_search_num; $c++) {
                if ($this->input->post('bSearchable_' . $c) !== FALSE and $this->input->post('sSearch_' . $c) !== FALSE and $this->input->post('sSearch_' . $c) != '') {
                    ${'search_' . $c} = $this->input->post('sSearch_' . $c);
                    $where .= ' AND ' . $column_search[$c] . ' LIKE "%' . $this->db->escape_like_str(${'search_' . $c}) . '%"';
                }
            }

            // Order By
            $order_by = NULL;
            if ($this->input->post('iSortCol_0') !== FALSE) {
                $iSortingCols = (int) $this->input->post('iSortingCols');
                for ($s = 0; $s < $iSortingCols; $s++) {
                    if ($s == 0) {
                        $order_by .= $column_sort[$this->input->post('iSortCol_' . $s)] . ' ' . strtoupper($this->input->post('sSortDir_' . $s));
                    } else {
                        $order_by .= ', ' . $column_sort[$this->input->post('iSortCol_' . $s)] . ' ' . strtoupper($this->input->post('sSortDir_' . $s));
                    }
                }
            }

            // Limit
            if ($this->input->post('iDisplayLength') != -1) {
                $limit = array($this->input->post('iDisplayLength'), $this->input->post('iDisplayStart'));
            } else {
                $limit = NULL;
            }

            // iTotalRecords
            $query_all_row = $this->db_model->getAllJoin($tb, $column_index, NULL, $init_where, NULL, NULL, NULL);
            $all_row = $query_all_row->num_rows();

            // Filter Data
            $this->db->select('SQL_CALC_FOUND_ROWS ' . $column_select, FALSE);
            $sevice_query = $this->db_model->getAllJoin($tb, NULL, NULL, $where, $order_by, $limit, NULL);

            // iTotalDisplayRecords
            $query_found_row = $this->db_model->getQuery('SELECT FOUND_ROWS() AS "found_row"');
            $found_row = $query_found_row->row()->found_row;

            // Output
            $data = array(
                'sEcho' => (int) $this->input->post('sEcho'),
                'iTotalRecords' => $all_row,
                'iTotalDisplayRecords' => $found_row,
                //'query' => $this->db->last_query(),
                'aaData' => array()
            );
            if ($sevice_query->num_rows() > 0) {
                foreach ($sevice_query->result() as $service) {
                    $row = array(
                        '0' => "<a href=" . base_url('manage_service/variant?id=' . $service->service_variant_id . ' ') . ">" . $service->variant . "</a>",
                        '1' => $service->price,
                        '2' => $service->detail
                    );
                    $data['aaData'][] = $row;
                }
            }
            echo $this->input->get('jsoncallback') . '(' . json_encode($data) . ')';
        } else {
            exit();
        }
    }

    public function add() {
        $data['detail'] = $this->service_model->get_category();
        $this->load->view('service_add', $data);
    }

    public function service_add() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_created" => $date_now,
            "date_modify" => $date_now,
            "service" => $this->input->post("inp_service"),
            "category" => $this->input->post("sl_category"),
            "price" => $this->input->post("inp_price")
        );
        if ($this->service_model->service_add($ar)) {
            redirect(base_url('service'));
        } else {
            alert('เกิดข้อผิดพลาด');
        }
    }

    public function category() {
        $this->load->view('service_category');
    }

    public function category_add() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_created" => $date_now,
            "date_modify" => $date_now,
            "category" => $this->input->post("inp_category")
        );
        if ($this->service_model->category_add($ar)) {
            redirect(base_url('service/category'));
        } else {
            echo ("<SCRIPT LANGUAGE='JavaScript'>
            window.alert('เกิดข้อผิดพลาด')
            window.location.href='" . base_url('service/category') . "';
            </SCRIPT>");
        }
    }
    
    public function edit_service_category(){
        $id = $this->input->get('id');
        $data['detail'] = $this->service_model->some_category($id);
        $this->load->view('edit_service_category',$data);
    }

}
