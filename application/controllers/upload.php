<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function index() {
        $this->load->view('upload');
    }

    public function do_upload() {

        $product_id = $this->input->post("product_id");

        $config = array("upload_path" => 'assets/img/upload/', "allowed_types" => "jpg|png", "max_size" => 2000, "max_width" => 2000);
        $this->load->library("upload", $config);

        if ($this->upload->do_upload("qqfile")) {
            $data = $this->upload->data();
            rename($data['full_path'], $data['file_path'] . date("YmdHis") . $data['file_ext']);
            $upload_data = $this->upload->data();
            $original_file_name = $upload_data['file_name'];

//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $data['file_path'] . date("YmdHis") . $data['file_ext'];
//            $config['maintain_ratio'] = false;
//
//            $config['maintain_ratio'] = false;
//            if ($data['image_width'] > $data['image_height']) {
//                $config['width'] = $data['image_height'];
//                $config['height'] = $data['image_height'];
//                $config['x_axis'] = (($data['image_width'] / 2) - ($config['width'] / 2));
//            } else {
//                $config['height'] = $data['image_width'];
//                $config['width'] = $data['image_width'];
//                $config['y_axis'] = (($data['image_height'] / 2) - ($config['height'] / 2));
//            }
//            $config['create_thumb'] = TRUE;
//            $new_filename = date("YmdHis") . '_thumb' . $data['file_ext'];
//            $this->load->library('image_lib', $config);
//            $this->image_lib->initialize($config);
//            if ($this->image_lib->crop()) {
//                $error = $this->image_lib->display_errors();
//            }
//            $this->image_lib->clear();
//            unset($config);
            $new_filename = date("YmdHis") . $data['file_ext'];
            date_default_timezone_set('Asia/Bangkok');
            $date_now = date('Y-m-d H:i:s');
            $data = array(
                "img" => $new_filename,
                "date_modify" => $date_now
            );
            $this->product_model->img_update($product_id,$data);
 
            $dt['filename'] = $new_filename;
            $dt['status'] = '1';
            $dt['success'] = TRUE;
            echo json_encode($dt);
        } else {
            $dt['status'] = '0';
            $dt['success'] = FALSE;
            echo json_encode($dt);
        }
    }

}
