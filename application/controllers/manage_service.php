<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manage_service extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'service';
        $this->load->model('service_model');
    }

    public function index() {
        $sevice_id = $this->input->get("id");
        $data['detail'] = $this->service_model->load_service($sevice_id);
        $this->load->view('edit_service', $data);
    }

    public function update_service() {
        $service_id = $this->input->post("service_id");
        $ar = array(
            "service" => $this->input->post("service"),
            "category" => $this->input->post("category"),
            "price" => $this->input->post("price")
        );
        $check = $this->service_model->update_service($service_id, $ar);
        if ($check) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function variant() {
        $variant_id = $this->input->get("id");
        $data['detail'] = $this->service_model->load_variant($variant_id);
        $this->load->view("edit_variant", $data);
    }

    public function update_variant() {
        $service_variant_id = $this->input->post("service_variant_id");
        $ar = array(
            "variant" => $this->input->post("variant"),
            "price" => $this->input->post("price"),
            "detail" => $this->input->post("detail")
        );
        $check = $this->service_model->update_variant($service_variant_id, $ar);
        if ($check) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function save_detail() {
        $product_id = $this->input->post("product_id");
        $ar_product = array(
            "product_name" => $this->input->post("product_name")
        );

        $old_variant = $this->input->post("old_variant");
        $ar_variant = array(
            "variant" => $this->input->post("variant"),
            "price" => $this->input->post("price"),
            "detail" => $this->input->post("detail")
        );
        //echo $this->input->post("detail");
        $check = $this->db_model->save_detail($product_id, $ar_product, $old_variant, $ar_variant);
        if ($check) {
            echo '1';
        } else {
            echo '2';
        }
    }

    public function edit() {
        $product_name = $this->input->get("n");
        $data['detail'] = $this->db_model->get_detail($product_name);
        $this->load->view("" . $product_name . "", $data);
    }

    public function product_detail() {
        $product_id = $this->input->post("product_id");
        $variant = $this->input->post("variant");
        //echo($product_id." ".$variant);
        $product_detail = $this->price->product_detail($product_id, $variant);
        echo json_encode($product_detail);
    }

    public function category() {
        $category = $this->service_model->get_category();
        echo json_encode($category);
    }

    public function update_category() {
        $id = $this->input->post("service_category_id");
        $ar = array(
            'category' => $this->input->post("category")
        );
        if ($this->service_model->update_category($id, $ar)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    

}
