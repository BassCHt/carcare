<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'dashboard';
        $this->load->model('db_model');
    }

    public function index() {
        //$data['detail'] = $this->db_model->quotation();
        //$data['history'] = $this->db_model->load_history();
        $this->load->view('dashboard');
    }
    

}
