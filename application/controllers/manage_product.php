<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manage_product extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'product';
        $this->load->model('product_model');
    }

    public function index() {
        
    }

    function some_category() {
        $cr_id = $this->input->post("cr_id");
        $data = $this->product_model->some_category($cr_id);
        echo json_encode($data);
    }

    function update_category() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $cr_id = $this->input->post("cr_id");
        $ar = array(
            "date_modify" => $date_now,
            "category" => $this->input->post("category")
        );
        if ($this->product_model->update_category($cr_id, $ar)) {
            echo '1';
        } else {
            echo '2';
        }
    }

    function category_add() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $ar = array(
            "date_created" => $date_now,
            "date_modify" => $date_now,
            "category" => $this->input->post("variant_name"),
            "store_id" => $this->input->post("store_id")
        );
        if ($last_id = ($this->product_model->category_add($ar))) {
            echo $last_id;
        } else {
            echo '2';
        }
    }
    
    
    function category_del() {
        $vr_id = $this->input->post("vr_id");
        if($this->product_model->category_del($vr_id)){
            echo '1';
        }else{
            echo '2';
        }
    }
    
    public function category() {
        $category = $this->service_model->get_category();
        echo json_encode($category);
    }
    
    public function check_category(){
        $product_id = $this->input->post("product_id");
        $data = $this->product_model->check_category($product_id);
        echo json_encode($data);
        //echo $product_id;
    }

}
