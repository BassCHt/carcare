<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->main_nav = 'register';
        $this->load->model('member_model');
    }

    public function index() {
        $this->load->view('register');
    }

    public function member_add() {
        date_default_timezone_set('Asia/Bangkok');
        $date_now = date('Y-m-d H:i:s');
        $pass = md5($this->input->post('password'));
        $ar = array(
            "date_created" => $date_now,
            "date_modify" => $date_now,
            "latest_login" => NULL,
            "username" => $this->input->post('username'),
            "password" => $pass,
            "email" => $this->input->post('email'),
            "level" => 0
        );
        if($this->member_model->save_member($ar)){
            echo '1';
        }else{
            echo '2';
        }        

    }

    

}
